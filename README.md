# Matplotlib Challenge

We can conclude from the mouse study results that:

* There is a direct linear relationship between tumor volume and weight
* Tumor volume decreases over time as mices are exposed to the capomulin drug treatment
* The final tumor volume appears to be smaller for mices undergone through the Capomulin and Remicane treatment compared to those exposed to Infubinol and Ceftamin
